package bibliotheque;
import bibliotheque.Volume;

public class BD extends Volume {
	//Attributs
	private String nomDessinateur;
	private String titre;

	
	//Getters et Setters
		public String getNomDessinateur() {
			return nomDessinateur;
		}
		/**
	 * @param nomDessinateur
	 * @param titre
	 */
	public BD( String titre, String auteur, String nomDessinateur) {
		super(auteur);
		this.nomDessinateur = nomDessinateur;
		this.titre = titre;
	}
		public void setNomDessinateur(String nomDessinateur) {
			this.nomDessinateur = nomDessinateur;
		}
		

		//Methodes
		/**
		 * Methode toString()
		 * @return Retourne le nom du dessinateur et le titre de la BD
		 */
		public String toString() {
			return (getNomDessinateur() + getTitre());
		}
		
		public void afficher() {
			System.out.println(toString());
		}
}
