package bibliotheque;

public abstract class Volume extends Document{
	
	//Attributs
	protected String auteur; 
	
	public Volume(String auteur) {
		this.auteur = auteur;
	}
	//Getters et Setters
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	
	//Methodes
	/**
	 * Methode toString()
	 * @return Retourne le nom de l'ecrivain et le titre de son livre
	 */
	
	public String toString() {
		return (getAuteur() + getTitre());
	}
	
	public void afficher() {
		System.out.println(toString());
	}

}
