package bibliotheque;

public class Adherent {
	
	//Attributs
	private String nom;
	private String prenom;
	
	//Constructeur
	public Adherent(String nom, String prenom){
		this.nom = nom;
		this.prenom = prenom;
	}
	
	//Getters et Setters
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	//Methodes
	/**
	 * Methode toString()
	 * @return Retourne le nom et le prenom d'un adherent
	 */
	public String toString() {
		return (getNom() + " " + getPrenom() );
	}
	public void afficher() {
		System.out.println(toString());
	}

	//void emprunter(Livre) {}
	//void rendre(Livre) {}
}
