package bibliotheque;

public abstract class Document {

	//Attributs
	private String titre;
	
	//Getters et Setters
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void empruntable() {}
}
