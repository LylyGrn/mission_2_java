package bibliotheque;

public class Journaux extends Document{
	private String titre;
	private int mois;
	private int annee;
	public Journaux(String titre, int mois, int annee) {
		super();
		this.titre = titre;
		this.mois = mois;
		this.annee = annee;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the mois
	 */
	public int getMois() {
		return mois;
	}
	/**
	 * @param mois the mois to set
	 */
	public void setMois(int mois) {
		this.mois = mois;
	}
	/**
	 * @return the annee
	 */
	public int getAnnee() {
		return annee;
	}
	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	public String toString() {
		return (getTitre() + ", " + mois + ", " + annee );
	}
		

}
