package bibliotheque;

import java.util.ArrayList;
import java.util.Vector;

public class Bibliotheque {
	
	
	ArrayList<Adherent> adherents;
	ArrayList<Document> documents;

	//Constructeur 
	public Bibliotheque(){
		adherents= new ArrayList<Adherent>();
		documents= new ArrayList<Document>();
	}
	
	//Methodes 
	public void ajouterAdherent(Adherent adherent){
		adherents.add(adherent);
	}
	public void ajouterDocument(Document document){
		documents.add(document);
	}
	
	public static void main(String[] args) {
		Bibliotheque bibli = new Bibliotheque();
		Adherent ad = new Adherent("Jean", "Dupond");
		bibli.ajouterAdherent(ad);
		ad = new Adherent("Marie", "Dubois");
		bibli.ajouterAdherent(ad);
		
		int n = 4;
		Document[] docs = new Document[n];
		docs[0] = new Livre("Miserables", "Hugo");
		docs[1] = new Journaux("Obs", 10, 2013);
		docs[2] = new BD("BDsqdss", "Thouard", "Dessinatereur");
		docs[3] = new Livre("zear", "Hezezrzaerzrugo");
		for (int i = 0; i<n; i++) {
			System.out.println(docs[i]);
			bibli.ajouterDocument(docs[i]);
			
			
		}

		ad.afficher();
	}

}
