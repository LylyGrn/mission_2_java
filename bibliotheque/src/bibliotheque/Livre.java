package bibliotheque;

public class Livre extends Volume{

	private String titre;

	
	
	public Livre(String titre, String auteur) {
		super(auteur);
		this.titre = titre;
		
	}


	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}


	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}


	/**
	 * @return the nom
	 */
	public String getAuteur() {
		return auteur;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setAuteur(String nom) {
		this.auteur = nom;
	}

	
	
	/*public boolean empruntable() {
		if() {
			
		}
	}*/
}
